FROM centos
RUN yum -y install epel-release
RUN yum -y install Pound
RUN rm /etc/pound.cfg
COPY pound.cfg /etc/pound.cfg
CMD ["pound"]